// import React, { Component } from 'react' // no longer need to destructure component
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// class UserItem extends Component {
//this does not have to be a class we are only passing in props

const UserItem = ({ user: { login, avatar_url, html_url } }) => {
  return (
    <div className="card text-center">
      <img src={avatar_url} alt="" className="round-img" style={{ width: '60px' }} />
      <h3>{login}</h3>
      <div>
        <Link to={`/user/${login}`} className="btn btn-dark btn-sm my-">
          More
        </Link>
      </div>
    </div>
  );
  // }
};

UserItem.prototype = {
  user: PropTypes.object.isRequired,
};

export default UserItem;
